﻿using UnityEngine;
using System;

namespace SOCore
{
    [CreateAssetMenu(fileName = "Currency", menuName = "SOCore/Currency", order = 200)]
    public class Currency : IntVariable 
    {
        [SerializeField] private IntVariable currencyToAcquire;
        [SerializeField] private IntReference multiplier;
	
        public void Add()
        {
            Value += currencyToAcquire.Value;
            currencyToAcquire.Value = 0;
        }

        public void AddWithMultiplier()
        {
            Value += currencyToAcquire.Value * multiplier.Value;
            currencyToAcquire.Value = 0;
        }

        public bool TryRemove(int amount)
        {
            if(Value >= amount)
            {
                Value -= amount;
                return true;
            }
            return false;
        }
    }
}

