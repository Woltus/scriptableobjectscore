﻿using System;
using UnityEngine;

namespace SOCore
{
    [CreateAssetMenu(fileName = "Skill", menuName = "SOCore/Skill", order = 201)]
    public class Skill : IntVariable 
    {
        public static event Action<Skill> SkillBought;

        [SerializeField] private Skill previousSkill;
        [SerializeField] private Currency skillPoints;
        [SerializeField] private int cost = 1, maxLevel = 1;
        [SerializeField] private Sprite icon;
        [SerializeField] private string displayName;
        [SerializeField][TextArea] private string description;

        public Sprite Icon => icon;
        public string DisplayName => displayName;
        public string Description => description;

        public bool Affordable => cost <= skillPoints.Value;
        public bool Maxed => Value >= maxLevel;
        public bool Unlocked 
        {
            get
            {
                if(previousSkill == null) return true;

                return previousSkill.Value > 0;
            }
        }

        public bool TryBuy()
        {
            if(skillPoints.TryRemove(cost))
            {
                Value++;
                SkillBought?.Invoke(this);
                return true;
            }
            return false;
        }
    }
}


