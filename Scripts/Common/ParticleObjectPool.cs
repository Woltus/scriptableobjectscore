﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SOCore
{
    [CreateAssetMenu(fileName = "ParticleObjectPool", menuName = "SOCore/Particle Object Pool", order = 300)]
    public class ParticleObjectPool : ScriptableObject 
    {
        [SerializeField] private ParticleSystem particlePrefab;
        [SerializeField][Range(0.1f, 10)] private float lifetime = 1;

        [NonSerialized] private Queue<ParticleSystem> _particles = new Queue<ParticleSystem>();

        private WaitForSeconds _waiting;

        private void OnEnable()
        {
            _waiting = new WaitForSeconds(lifetime);
        }

        public IEnumerator Play(Vector3 position)
        {
            ParticleSystem particle = GetParticle();
            particle.transform.position = position;
            particle.Play();
            yield return _waiting;
            particle.Stop();
            _particles.Enqueue(particle);
        }

        private ParticleSystem GetParticle()
        {
            if(_particles.Count < 1)
            {
                return Instantiate(particlePrefab);
            }
            
            ParticleSystem particle = _particles.Dequeue();
            if(particle.Equals(null))
            {
                _particles.Clear();
                return Instantiate(particlePrefab);
            }

            return particle;
        }
    }
}


