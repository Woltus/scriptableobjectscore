﻿using UnityEngine;
using System;

namespace SOCore
{  
    [CreateAssetMenu(fileName = "new Event", menuName = "SO Core/Event", order = 100)]
    public class Event : ScriptableObject
    {
        public event Action Invoked;

        public void Invoke()
        {
            Debug.Log($"{name} Invoked");
            Invoked?.Invoke();
        }
    }
}


