﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SOCore
{
    [CreateAssetMenu(fileName = "New StringVariable", menuName = "SO Core/String Variable", order = 3)]
    public class StringVariable : SaveableVariable<string> 
    {
        protected override void Load()
        {
            _value = PlayerPrefs.GetString(saveKey, _value);
        }

        protected override void Save()
        {
            PlayerPrefs.SetString(saveKey, Value);
        }

        #if UNITY_EDITOR
        public static void ResetAll()
        {
            List<StringVariable> variablesToReset = new List<StringVariable>();
            string [] guids = AssetDatabase.FindAssets("t:" + typeof(StringVariable).Name, new[] {"Assets/ScriptableObjects"});
            foreach (var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                variablesToReset.Add(AssetDatabase.LoadAssetAtPath<StringVariable>(path));
            }
            foreach (var variable in variablesToReset)
            {
                if(variable.Resetable)
                {
                    variable.ResetValue();
                    EditorUtility.SetDirty(variable);
                }
            }

            AssetDatabase.SaveAssets();   
        }
        #endif
    }
}


