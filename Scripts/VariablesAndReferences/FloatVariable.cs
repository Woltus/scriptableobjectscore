﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SOCore
{
    
    [CreateAssetMenu(fileName = "New FloatVariable", menuName = "SO Core/Float Variable", order = 2)]
    public class FloatVariable : SaveableVariable<float> 
    {
        protected override void Load()
        {
            _value = PlayerPrefs.GetFloat(saveKey, baseValue);
        }

        protected override void Save()
        {
            PlayerPrefs.SetFloat(saveKey, Value);
        }

        #if UNITY_EDITOR
        public static void ResetAll()
        {
            List<FloatVariable> variablesToReset = new List<FloatVariable>();
            string [] guids = AssetDatabase.FindAssets("t:" + typeof(FloatVariable).Name, new[] {"Assets/ScriptableObjects"});
            foreach (var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                variablesToReset.Add(AssetDatabase.LoadAssetAtPath<FloatVariable>(path));
            }
            foreach (var variable in variablesToReset)
            {
                if(variable.Resetable)
                {
                    variable.ResetValue();
                    EditorUtility.SetDirty(variable);
                }
            }

            AssetDatabase.SaveAssets();
        }

        [ContextMenu("Set name as save key")]
        public void SetNameAsSaveKey()
        {
            saveKey = name;
        }

        #endif
    }
}


