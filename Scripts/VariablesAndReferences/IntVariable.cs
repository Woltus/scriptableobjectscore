using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SOCore
{
    [CreateAssetMenu(fileName = "New IntVariable", menuName = "SO Core/Int Variable", order = 1)]
    public class IntVariable : SaveableVariable<int>
    {   
        protected override void Load()
        {
            _value = PlayerPrefs.GetInt(saveKey, baseValue);
        }

        protected override void Save()
        {
            PlayerPrefs.SetInt(saveKey, Value);
        }

        public void Increment()
        {
            Value++;
        }
        
        #if UNITY_EDITOR
        public static void ResetAll()
        {
            List<IntVariable> allVariables = new List<IntVariable>();
            string [] guids = AssetDatabase.FindAssets("t:" + typeof(IntVariable).Name, new[] {"Assets/ScriptableObjects"});
            foreach (var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                allVariables.Add(AssetDatabase.LoadAssetAtPath<IntVariable>(path));
            }
            foreach (var variable in allVariables)
            {
                if(variable.Resetable)
                {
                    variable.ResetValue();
                    EditorUtility.SetDirty(variable);
                }
            }

            AssetDatabase.SaveAssets();
        }
        #endif
    }
}
