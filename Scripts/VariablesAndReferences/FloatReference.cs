﻿using System;

namespace SOCore
{
    [Serializable]
    public class FloatReference : Reference<float, FloatVariable> {}
}