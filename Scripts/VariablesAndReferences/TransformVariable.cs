﻿using UnityEngine;

namespace SOCore
{
    [CreateAssetMenu(fileName = "New TransformVariable", menuName = "SO Core/TransformVariable", order = 4)]

    public class TransformVariable : BaseVariable<Transform>
    {

    }
}


