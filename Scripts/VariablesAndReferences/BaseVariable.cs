﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SOCore
{
    public abstract class BaseVariable<T> : ScriptableObject
    {
        public event Action<T> ValueChanged; 
        [SerializeField] protected T _value;
        [SerializeField] protected bool resetable;
        [SerializeField] protected T baseValue;
        
        public virtual T Value 
        { 
            get => _value; 
            set 
            {
                _value = value;
                OnValueChange ();
                ValueChanged?.Invoke(value);
            }
        }
        
        public bool Resetable => resetable;

        protected virtual void OnValueChange () {}

        #if UNITY_EDITOR

        public void ResetValue() 
        {
            _value = baseValue;    
        }

        #endif
    }
}


