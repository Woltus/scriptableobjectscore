﻿using UnityEngine;

namespace SOCore
{
    public abstract class Reference<T, V> : BaseReference where V : BaseVariable<T>
    {
        [SerializeField] private bool useConstant = true;
        [SerializeField] private T constantValue;
        [SerializeField] private V variable;
        
        public T Value 
        { 
            get => useConstant ? constantValue : variable.Value; 
            set 
            {
                if(useConstant)
                {
                    constantValue = value;
                }
                else
                {
                    variable.Value = value;
                }
            }
        }
        public V Variable => variable;

        public static implicit operator T(Reference<T, V> reference) => reference.Value;

        public void UseConstant (T value) {
            useConstant = true;
            constantValue = value;
        }

        public void UseVariable (V variableToUse)
        {
            variable = variableToUse;
            useConstant = false;
        }
    }
}


