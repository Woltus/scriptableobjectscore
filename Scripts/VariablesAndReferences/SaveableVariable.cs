﻿using System.Collections.Generic;
using UnityEngine;

namespace SOCore
{
    public abstract class SaveableVariable<T> : BaseVariable<T>
    {
        [SerializeField] protected bool saveable;
        [SerializeField] protected string saveKey;

        private static Dictionary<string, BaseVariable<T>> _saveKeys = new Dictionary<string, BaseVariable<T>>();        

        public bool WasSaved => PlayerPrefs.HasKey(saveKey);

        private void OnEnable() 
        {
            if(saveable)
            {
                if(AssertSaveKey())
                {
                    Load();
                }
            }
        }

        protected override void OnValueChange () 
        {
            if(saveable)
            {
                Save();
            }
        }

        protected abstract void Save();
        protected abstract void Load();

        private bool AssertSaveKey()
        {
            if(string.IsNullOrEmpty(saveKey))
            {
                Debug.LogError($"{name} variable is set as saveable, but save key is empty");
                saveable = false;
                return saveable;
            }
            if(_saveKeys.ContainsKey(saveKey))
            {
                if(_saveKeys[saveKey] != this)
                {
                    Debug.LogError($"{name} variable is set as saveable, but save key is already used on {_saveKeys[saveKey].name}");
                    return saveable;
                }
                return true;  
            }

            _saveKeys.Add(saveKey, this);
            return true;
        }
    }
}


