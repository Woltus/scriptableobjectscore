﻿using System;

namespace SOCore
{
    [Serializable]
    public class BoolReference : Reference<bool, BoolVariable> {}
}
