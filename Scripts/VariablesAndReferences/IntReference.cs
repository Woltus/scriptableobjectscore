﻿using System;

namespace SOCore
{
    [Serializable]
    public class IntReference : Reference<int, IntVariable> {}
}