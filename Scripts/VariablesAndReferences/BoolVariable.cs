﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SOCore
{
    [CreateAssetMenu(fileName = "New BoolVariable", menuName = "SO Core/Bool Variable", order = 0)]
    public class BoolVariable : SaveableVariable<bool> 
    {
        protected override void Load()
        {
            string boolData = PlayerPrefs.GetString(saveKey, baseValue.ToString());
            if(!bool.TryParse(boolData, out _value))
            {
                Debug.LogError($"Can't parse saved data on {name} variable");
            }
        }

        protected override void Save()
        {
            PlayerPrefs.SetString(saveKey, Value.ToString());
        }

        #if UNITY_EDITOR
        public static void ResetAll()
        {
            List<BoolVariable> variablesToReset = new List<BoolVariable>();
            string [] guids = AssetDatabase.FindAssets("t:" + typeof(BoolVariable).Name, new[] {"Assets/ScriptableObjects"});
            foreach (var guid in guids)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                variablesToReset.Add(AssetDatabase.LoadAssetAtPath<BoolVariable>(path));
            }
            foreach (var variable in variablesToReset)
            {
                if(variable.Resetable)
                {
                    variable.ResetValue();
                    EditorUtility.SetDirty(variable);
                }
            }

            AssetDatabase.SaveAssets();    
        }  
        #endif
    }
}