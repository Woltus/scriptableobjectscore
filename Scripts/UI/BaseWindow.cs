﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SOCore.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class BaseWindow : MonoBehaviour
    {
        [SerializeField] private UnityEvent OnStartShowing, OnShowed;
        [SerializeField] private float animationSpeed = 1, showDelay, hideDelay;
        [SerializeField] private List<SOCore.Event> showEvents = new List<SOCore.Event>();
        [SerializeField] private List<SOCore.Event> hideEvents = new List<SOCore.Event>();
        [SerializeField] private bool hideOnAwake;

        public bool IsShowing => _showing;

        private bool _showing, _hiding;
        private CanvasGroup _canvasGroup;

        protected virtual void Awake() 
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            foreach (var ev in showEvents)
            {
                ev.Invoked += Show;
            }

            foreach (var ev in hideEvents)
            {
                ev.Invoked += Hide;
            }
            if(hideOnAwake)
            {
                HideImediate();
            }
        }

        protected virtual void OnDestroy() 
        {
            foreach (var ev in showEvents)
            {
                ev.Invoked -= Show;
            }

            foreach (var ev in hideEvents)
            {
                ev.Invoked -= Hide;
            }
        }

        public virtual void Show()
        {
            if(_showing) return;
            StopAllCoroutines();
            StartCoroutine(Showing());
        }

        public virtual void Hide()
        {
            if(_hiding) return;
            StopAllCoroutines();
            StartCoroutine(Hiding());
        }

        private IEnumerator Showing()
        {
            _hiding = false;
            _showing = true;
            if(showDelay > float.Epsilon)
            {
                yield return new WaitForSeconds(showDelay);
            }
            OnStartShowing.Invoke();
            float delta = _canvasGroup.alpha;
            _canvasGroup.blocksRaycasts = true;
            while(delta < 1)
            {
                delta += animationSpeed * Time.deltaTime;
                _canvasGroup.alpha = delta;
                yield return null;
            }
            _canvasGroup.alpha = 1;
            _canvasGroup.interactable = true;
            _showing = false;
            OnShowed.Invoke();
        }

        private IEnumerator Hiding()
        {
            _canvasGroup.interactable = false;
            _canvasGroup.blocksRaycasts = false;
            _showing = false;
            _hiding = true;
            if(hideDelay > float.Epsilon)
            {
                yield return new WaitForSeconds(hideDelay);
            }
            float delta = _canvasGroup.alpha;
            
            while(delta > 0)
            {
                delta -= animationSpeed * Time.deltaTime;
                _canvasGroup.alpha = delta;
                yield return null;
            }
            _canvasGroup.alpha = 0;
            _hiding = false;
            HideImediate();
        }

        protected void HideImediate()
        {
            _canvasGroup.alpha = 0;
            _canvasGroup.interactable = false;
            _canvasGroup.blocksRaycasts = false;
        }
    }
}


