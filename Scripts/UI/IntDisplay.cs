﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace SOCore.UI
{
    public class IntDisplay : MonoBehaviour 
    {
        [SerializeField] private TextMeshProUGUI label;
        [SerializeField] private IntVariable variable;
        [SerializeField] private IntReference offset;
        [SerializeField] private IntReference multiplier;
        [SerializeField] private string prefix = "", suffix = "";
        [SerializeField] private bool animate;
        [SerializeField] private float animationSpeed = 1;
        [SerializeField] private bool autoUpdate = true;
        
        private int _lastValue;

        private int Value => (variable.Value * multiplier) + offset;
        
        private void Awake() 
        {
            if(autoUpdate)
            {   
                variable.ValueChanged += UpdateDisplay;
            }
            UpdateText(Value);
            _lastValue = Value;
        }

        private void OnDestroy() 
        {
            variable.ValueChanged -= UpdateDisplay;
        }
        
        public void UpdateDisplay(int value)
        {
            if(animate)
            {
                StartCoroutine(Animating());
            }
            else
            {
                UpdateText(Value);
                _lastValue = Value;
            }
        }

        public void ClearDisplay()
        {
            UpdateText(0);
            _lastValue = 0;
        }

        private void UpdateText(int value)
        {
            label.text = $"{prefix}{value}{suffix}";
        }

        private IEnumerator Animating()
        {
            float delta = 0;
            while (delta <= 1)
            {
                int value = (int)Mathf.Lerp(_lastValue, Value, delta);
                UpdateText(value);
                delta += Time.deltaTime * animationSpeed;
                yield return null;
            }
            UpdateText(Value);
            _lastValue = Value;
        }
    }
}

